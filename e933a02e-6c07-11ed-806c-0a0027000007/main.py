# coding=utf-8
from __future__ import print_function, absolute_import

from gm.api import *

if __name__ == '__main__':
    '''
        strategy_id策略ID, 由系统生成
        filename文件名, 请与本文件名保持一致
        mode运行模式, 实时模式:MODE_LIVE回测模式:MODE_BACKTEST
        token绑定计算机的ID, 可在系统设置-密钥管理中生成
        backtest_start_time回测开始时间
        backtest_end_time回测结束时间
        backtest_adjust股票复权方式, 不复权:ADJUST_NONE前复权:ADJUST_PREV后复权:ADJUST_POST
        backtest_initial_cash回测初始资金
        backtest_commission_ratio回测佣金比例
        backtest_slippage_ratio回测滑点比例
    '''
    run(strategy_id='e933a02e-6c07-11ed-806c-0a0027000007',
        filename='AIT-自动交易-超短-七步尾盘买入法V2.py',
        mode=MODE_BACKTEST,
        token='883a7fc43696f3cc100c78b423357976b2ec3352',
        backtest_start_time='2022-11-01 08:00:00',
        backtest_end_time='2022-11-10 16:00:00',
        backtest_adjust=ADJUST_PREV,
        backtest_initial_cash=10000000,
        backtest_commission_ratio=0.0001,
        backtest_slippage_ratio=0.0001)
