# coding=utf-8
from __future__ import print_function, absolute_import

import os
from typing import NoReturn, Text

import pywencai
# from gmcache import *  # 回测环境使用缓存API,实时运行的时候再改回去
from gm.api import *
from gm.csdk.c_sdk import TickLikeDict2
from gm.model import DictLikeAccountStatus, DictLikeIndicator, DictLikeOrder, DictLikeParameter
from retry import retry

from python_quant.common.code import *
from python_quant.common.log import get_logger
from python_quant.quant_api.juejin.order import *
from python_quant.stock.fenshi import *

"""
本示例用于说明python sdk 当前支持的回调方法示例. 
不具有业务含义, 只用于策略编写参考
注：
建议使用python3.6.5以上的版本, gmsdk 支持Python3.6.x, python3.7.x, python3.8.x, python3.9.x 
"""

log = get_logger(os.path.split(__file__)[-1].split(".")[0])


def init(context):
    """
    策略中必须有init方法,且策略会首先运行init定义的内容，可用于
    * 获取低频数据(get_fundamentals, get_fundamentals_n, get_instruments, get_history_instruments, get_instrumentinfos,
    get_constituents, get_history_constituents, get_sector, get_industry, get_trading_dates, get_previous_trading_date,
    get_next_trading_date, get_dividend, get_continuous_contracts, history, history_n, )
    * 申明订阅的数据参数和格式(subscribe)，并附带数据事件驱动功能
    * 申明定时任务(schedule)，附带本地时间事件驱动功能
    * 读取静态的本地数据或第三方数据
    * 定义全局常量,如 context.user_data = 'balabala'
    * 最好不要在init中下单(order_volume, order_value, order_percent, order_target_volume, order_target_value, order_target_percent)
    """

    add_parameters(context)
    unsubscribe(symbols='*', frequency='*')
    context.stock_list = list()
    # 示例定时任务: 每天 14:50:00 调用名字为 my_schedule_task 函数
    schedule(schedule_func=select_stocks, date_rule='1d', time_rule='14:50:00')

    subscribe(symbols="SHSE.000300", frequency='tick', count=1, wait_group=True)


def add_parameters(context):
    context.per_amount_to_trade = 10000
    add_parameter(key='per_amount_to_trade', value=context.per_amount_to_trade, min=1000, max=100000,
                  name='每次买入的金额', intro='每次买入的金额', group='1',
                  readonly=False)
    context.is_full_up_real_price = 2
    add_parameter(key='is_full_up_real_price', value=context.is_full_up_real_price, min=1, max=2,
                  name='是否启用全天股价运行在分时线上方', intro='是否启用全天股价运行在分时线上方，1是2否', group='1',
                  readonly=False)


@retry(tries=-1, delay=3, backoff=2, max_delay=60)
def select_stocks(context):
    print(context.now)
    """
    定时任务函数
    注意: 这类的函数可以自定义函数名, 但是只能有一个context做为参数
    """
    response = get_wen_cai()

    code_list = list()
    if response is not None and 'code' in response:
        array1 = response['code']
        list1 = array1.tolist()
        code_list.extend(list1)

    full_code_list = list()
    # 当前时间
    now_date = context.now.strftime("%Y-%m-%d")
    for code in code_list:
        add_to_list = True
        if context.is_full_up_real_price == 1:
            fenshi_data_df = jue_jin_fen_shi_context_data(context, code, now_date)
            is_full_up = is_full_price_up_fen_shi(fenshi_data_df)
            if is_full_up is False:
                add_to_list = False

        if add_to_list is True:
            jue_jin_code = code_to_shse_szse_symbol(code)
            full_code_list.append(jue_jin_code)

    context.stock_list = full_code_list
    subscribe(symbols=full_code_list, frequency='60s', count=1000, unsubscribe_previous=True, wait_group=True)


def get_wen_cai():
    question1 = "涨幅在3%到5%的沪深A股，非st，非停牌，量比大于1，换手率大于等于5%小于等于10%，流通市值大于50亿小于200亿，成交量温和放大, 非科创板"
    response1 = pywencai.get(question=question1, loop=True)

    question2 = "长上影线股，高位"
    response2 = pywencai.get(question=question2, loop=True)
    if len(response1) == 0:
        return
    # 找到交集
    response1_in = response1[response1["code"].isin(response2["code"])]
    # 差集
    response = pd.concat([response1, response1_in, response1_in]).drop_duplicates(subset=['code'], keep=False)
    return response


def on_tick(context, tick):
    # type: (Context, TickLikeDict2) -> NoReturn
    """
    tick数据推送事件
    参数 tick 为当前被推送的tick.
    tick包含的key值有下列值.
    symbol              str                   标的代码
    open                float                 日线开盘价
    high                float                 日线最高价
    low                 float                 日线最低价
    price               float	              最新价
    cum_volume          long                  成交总量/最新成交量,累计值
    cum_amount          float                 成交总金额/最新成交额,累计值
    trade_type          int                   交易类型 1: ‘双开’, 2: ‘双平’, 3: ‘多开’, 4: ‘空开’, 5: ‘空平’, 6: ‘多平’, 7: ‘多换’, 8: ‘空换’
    last_volume         int                   瞬时成交量
    cum_position        int                   合约持仓量(期),累计值（股票此值为0）
    last_amount         float                 瞬时成交额
    created_at          datetime.datetime     创建时间
    quotes              list[Dict]            股票提供买卖5档数据, list[0]~list[4]分别对应买卖一档到五档, 期货提供买卖1档数据, list[0]表示买卖一档. 目前期货的 list[1] ~ list[4] 值是没有意义的
        quotes 里每项包含的key值有:
          bid_p:  float   买价
          bid_v:  int     买量
          ask_p   float   卖价
          ask_v   int     卖量

    注: 可以使用属性访问的方式得到相应的key的值. 如要访问: symbol. 则可以使用 tick.symbol 或 tick['symbol']
    访问quote里的bid_p, 则可以使用 tick.quotes[0].bid_p  或 tick['quotes'][0]['bid_p']
    """
    if tick.symbol == 'SHSE.000300':
        orders = auto_tp_sl_all_can_close_orders_market(context, 3, -3)
        if len(orders) > 0:
            log.info("完成止损止盈", orders)


def on_bar(context, bars):
    """
    bar数据推送事件
    参数 bars 为当前被推送的bar列表. 在调用subscribe时指定 wait_group=True, 则返回的是到当前已准备好的bar列表; 若 wait_group=False, 则返回的是当前推送的 bar 一个对象, 放在在 list 里
    bar 对象包含的key值有下列值.
    symbol         str      标的代码
    frequency      str      频率, 支持多种频率. 要把时间转换为相应的秒数. 如 30s, 60s, 300s, 900s
    open           float    开盘价
    close          float    收盘价
    high           float    最高价
    low            float    最低价
    amount         float    成交额
    volume         long     成交量
    position       long     持仓量（仅期货）
    bob            datetime.datetime    bar开始时间
    eob            datetime.datetime    bar结束时间

    注: 可以使用属性访问的方式得到相应的key的值. 如要访问: symbol. 则可以使用 bar.symbol 或 bar['symbol']
    """
    now_date = context.now.strftime("%Y-%m-%d")

    for bar in bars:
        if bar.symbol in context.stock_list:
            sub_data = jue_jin_fen_shi_context_data(context, bar.symbol, now_date)
            account_positions = context.account().positions()
            if is_afternoon_collection_bidding(context) is False & had_ordering(bar.symbol) is False & (len(
                    account_positions) < 10) & (bar.high >= max(sub_data['high'])) and had_position(context,
                                                                                                    bar.symbol) is False & (
                    bar.high > sub_data['avg'].loc[-1:]):
                orders = do_order_at_market(bar.symbol, context.per_amount_to_trade)
                if len(orders) != 0:
                    log.info(orders[0])


def on_order_status(context, order):
    # type: (Context, DictLikeOrder) -> NoReturn
    """
    委托状态更新事件. 参数order为委托信息
    响应委托状态更新事情，下单后及委托状态更新时被触发
    """
    pass


def on_execution_report(context, execrpt):
    """
    委托执行回报事件. 参数 execrpt 为执行回报信息
    响应委托被执行事件，委托成交后被触发
    """
    pass


def on_account_status(context, account_status):
    # type: (Context, DictLikeAccountStatus) -> NoReturn
    """
    交易账户状态变更事件. 仅响应 已连接，已登录，已断开 和 错误 事件
    account_status: 包含account_id(账户id), account_name(账户名),ConnectionStatus(账户状态)
    """
    pass


def on_parameter(context, parameter):
    # type: (Context, DictLikeParameter) -> NoReturn
    """
    动态参数修改事件推送. 参数 parameter 为动态参数的信息
    """
    if parameter.key == "per_amount_to_trade":
        context.per_amount_to_trade = parameter.value

    if parameter.key == "is_full_up_real_price":
        context.is_full_up_real_price = parameter.value


def on_backtest_finished(context, indicator):
    # type: (Context, DictLikeIndicator) -> NoReturn
    """
    回测结束事件. 参数 indicator 为此次回测的绩效指标参数信息

    """
    pass


def on_error(context, code, info):
    # type: (Context, int, Text) -> NoReturn
    """
    底层sdk出错时的回调函数
    :param context:
    :param code: 错误码.  参考: https://www.myquant.cn/docs/python/python_err_code
    :param info: 错误信息描述
    """
    pass


def on_trade_data_connected(context):
    # type: (Context) -> NoReturn
    """
    交易通道网络连接成功事件
    """
    pass


def on_market_data_connected(context):
    # type: (Context) -> NoReturn
    """
    实时行情网络连接成功事件
    """
    pass


def on_market_data_disconnected(context):
    # type: (Context) -> NoReturn
    """
    实时行情网络连接断开事件
    """
    pass


def on_trade_data_disconnected(context):
    # type: (Context) -> NoReturn
    """
    交易通道网络连接断开事件
    """
    pass


def on_shutdown(context):
    # type: (Context) -> NoReturn
    """
    策略退出前回调
    注：只有在终端点击策略·断开·按钮才会触发，直接关闭策略控制台不会被调用
    """
    pass
