import time
from xtquant import xtconstant
from xtquant.xttrader import XtQuantTrader
from xtquant.xttype import StockAccount

from TradeCallback import *

# path 为mini qmt客户端的userdata_mini路径

path = r'F:\gszqqmt\userdata_mini'

# session_id 为会话编号,策略使用方对于不同的Python策略需要使用不同的会话编号
session_id = int(time.time())

# 创建交易API实例
xt_trader = XtQuantTrader(path, session_id)
# print(xt_trader)
acc = StockAccount('这里输入你的账号')
# 创建交易回调对象,并声明接收回调
callback = MyXtQuantTraderCallback()
xt_trader.register_callback(callback)
# 启动交易线程
xt_trader.start()
# 建立交易连接,返回0表示成功
connect_result = xt_trader.connect()
if connect_result != 0:
    sys.exit('连接QMTmin失败,程序将退出 %d' % connect_result)

if __name__ == '__main__':
    stock_code = '600050.SH'  # 证券代码
    order_type = xtconstant.STOCK_BUY  # xtconstant.STOCK_BUY:证券买入  xtconstang.STOCK_SELL: 证券卖出
    price_type = xtconstant.FIX_PRICE  # xtconstant.LATEST_PRICE:最新价 xtconstant.FIX_PRICE:限价
    order_volume = 200  # order_volume: 委托数量,股票以'股' 为单位,债券以'张'为单位
    price = 7.29  # price:报价价格,如果price_type 为指定价,那price须指定价格,否则为0.
    strategy_name = 'strategy_name'  # strategy_name:策略名称
    order_remark = 'remark'  # order_remark : 委托备注

    # 同步下单报单
    seq = xt_trader.order_stock(acc, stock_code, order_type, order_volume, price_type, price,
                                'strategy_name', 'remark')
    print("下单编号:", seq)
    # 根据订单号查询委托
    orders = xt_trader.query_stock_orders(acc, seq)
