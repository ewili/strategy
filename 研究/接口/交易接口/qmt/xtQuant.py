import _thread

from xtquant import xtdata
from xtquant.xtdata import download_history_data, get_market_data


def on_progress(data):
    print(data + "+++++")


def on_data(datas):
    for stock_code in datas:
        # print(stock_code, datas[stock_code])
        break


def subscribe_whole_quotes(thread_name):
    all_stock_list = xtdata.get_stock_list_in_sector("沪深A股")
    whole_id = xtdata.subscribe_whole_quote(all_stock_list, callback=on_data)
    xtdata.run()


_thread.start_new_thread(subscribe_whole_quotes, ("subscribe_whole_quotes",))

stock_list = ['603909.SH']
field_list = ['time', 'open', 'close', 'low', 'high', 'volume']  # 提取的字段

download_history_data(stock_list, period='1d', start_time='20230201', end_time='20230223')
print('download_history_data2 finished')

# 获取股票close数据
ret = get_market_data(field_list, stock_list, period='1d', start_time='', end_time='', count=5,
                      dividend_type='front', fill_data=True)
print(ret)
