import os
import sys

from apscheduler.executors.pool import ProcessPoolExecutor, ThreadPoolExecutor

from config import strategy_config
from service import notify_mail

sys.path.append(strategy_config.python_quant_path)
from python_quant.common.log import get_logger
from apscheduler.schedulers.blocking import BlockingScheduler

from strategy_module.new_stock_filter_strategy import NewStockFilterStrategy

log = get_logger(os.path.split(__file__)[-1].split(".")[0])


# log = get_logger("li_select")


def do_select_stock_filter():
    new_stock = NewStockFilterStrategy()
    try:
        stocks = new_stock.do_select()
        notify_mail.send_mail_thread("今日股票池", str(stocks))
    except Exception as result:
        log.error(result)

    try:
        new_stock.do_update()
        notify_mail.send_mail_thread("股票池支撑压力位更新完毕", "股票池支撑压力位更新完毕")
    except Exception as result:
        log.error(result)


if __name__ == '__main__':
    do_select_stock_filter()
    executor_process = ProcessPoolExecutor()
    executor_thread_pool = ThreadPoolExecutor()

    executors = {
        'default': executor_process,
        'executor_thread_pool': executor_thread_pool
    }

    scheduler = BlockingScheduler(executors=executors, timezone='Asia/Shanghai')

    scheduler.add_job(do_select_stock_filter, trigger="cron", day_of_week="*", hour=22, minute=57,
                      executor='executor_thread_pool')
    scheduler.start()
