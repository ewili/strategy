import sys
import threading

from config import strategy_config

sys.path.append(strategy_config.python_quant_path)
from python_quant.common.mail import SendMail
from python_quant.config.sys_config import NotifyMailConfig


def send_mail_thread(title, content: str):
    mail_send = SendMail(smtp_receiver=NotifyMailConfig.toaddrs, smtp_subject=title, smtp_body=content)
    threading.Thread(target=mail_send.send_mail()).start()
