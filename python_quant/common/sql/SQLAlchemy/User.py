from sqlalchemy import Column

from python_quant.common.sql.SQLAlchemy.BaseModel import BaseModel
from python_quant.common.sql.SQLAlchemy.SQLAlchemyHelper import Base_Decla


class User(Base_Decla, BaseModel):
    __tablename__ = "user"

    name = Column()
    phone = Column()
