from sqlalchemy import Column

from python_quant.common.sql.SQLAlchemy.BaseModel import BaseModel
from python_quant.common.sql.SQLAlchemy.SQLAlchemyHelper import Base_Decla


class Cool(Base_Decla, BaseModel):
    __tablename__ = "cool"
    name = Column()
