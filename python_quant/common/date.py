from datetime import timedelta, datetime, date

import akshare as ak
from gm.api import get_previous_trading_date, get_trading_dates


def date_to_str(date, parttern):
    """
    日期转string
    :param date:
    :param parttern:
    :return:
    """
    date_str = datetime.strftime(date, parttern)
    return date_str


def str_to_date(date_str, parttern):
    """
    string转日期
    :param date_str:
    :param parttern:
    :return:
    """
    date = datetime.strptime(date_str, parttern).date()
    return date


def between_days(day_before, day_after):
    """
    两个日期相隔天数
    :param day_before:
    :param day_after:
    :return:
    """
    return (day_after - day_before).days


def date_before_x_days_date(date, days, parttern):
    """
    指定日期之前多少天的日期
    :param date:
    :param days:
    :param parttern:
    :return:
    """
    return (date - timedelta(days=days)).strftime(parttern)


def is_today(date_now):
    """
    是不是今天
    :param date_now:
    :return: bool
    """
    # Get the year, month and day
    c_year = datetime.now().year
    c_month = datetime.now().month
    c_day = datetime.now().day
    # Disassemble the date
    date_list = date_now.split(" ")[0].split("-")
    t_year = int(date_list[0])
    t_month = int(date_list[1])
    t_day = int(date_list[2])
    final = False
    if c_year == t_year and c_month == t_month and c_day == t_day:
        final = True
    return final


def date_format(date_str, partern_old, partern_new):
    """
    格式化日期
    :param date_str:
    :param partern_old:
    :param partern_new:
    :return: date
    """
    date_old = datetime.strptime(date_str, partern_old)
    return datetime.strftime(date_old, partern_new)


def jue_jin_yesterday_trading_date(context):
    """
    上一个交易日
    :param context:
    :return:
    """
    now_date = context.now.strftime("%Y%m%d")
    return jue_jin_previous_trading_date(now_date)


def jue_jin_previous_trading_date(now_date):
    """
    上一个交易日
    :param now_date:
    :return:
    """
    now_date = datetime.strptime(now_date, "%Y%m%d")
    pre_date = get_previous_trading_date(exchange='SZSE', date=now_date)
    yes = datetime.strptime(pre_date, '%Y-%m-%d')
    yes = datetime.strftime(yes, "%Y%m%d")
    return yes


def jue_jin_is_today_trading_date(context):
    """
    今天是否是交易日
    """

    date3 = context.now.strftime("%Y-%m-%d")
    is_trading_date = get_trading_dates(exchange='SZSE', start_date=date3, end_date=date3)
    if len(is_trading_date) == 0:
        return False
    return True


def jue_jin_get_nearly_trading_date(context):
    """
    得到最近的交易日
    :param context:
    :return:
    """
    bool_is_today_trading_date = jue_jin_is_today_trading_date(context)
    if bool_is_today_trading_date is True:
        return context.now.strftime("%Y%m%d")
    return jue_jin_yesterday_trading_date(context)


def jue_jin_between_trading_days(start_date, end_date):
    """
        相差多少个交易日
    """
    trading_dates = get_trading_dates(exchange='SZSE', start_date=start_date, end_date=end_date)
    return len(trading_dates)


def last_trade_date():
    """
        最后交易日
    """
    today = date.today()
    trade_date_df = ak.tool_trade_date_hist_sina()
    trade_date_list = trade_date_df["trade_date"].astype(str).tolist()
    while str(today) not in trade_date_list:  # 如果当前日期不在交易日期列表内，则当前日期天数减一
        today = today - datetime.timedelta(days=1)
    return today


def is_same_day(data_a, data_b):
    if data_a.year == data_b.year and data_a.month == data_b.month and data_a.day == data_b.day:
        return True
    return False
