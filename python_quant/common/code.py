import operator


def code_to_sz_sh_symbol(code):
    """
    股票代码转换
    :param code:
    :return:
    """
    if code.startswith('00') or code.startswith('30'):
        code = 'sz' + code
    if code.startswith('6'):
        code = 'sh' + code
    return code


def code_remove_sz_sh_symbol(code):
    """
    股票代码转换
    :param code:
    :return:
    """
    if code.startswith('sz'):
        code = code.replace('sz', "")
    if code.startswith('sh'):
        code = code.replace('sh', "")
    return code


def code_to_shse_szse_symbol(code):
    if code.startswith('6'):
        code = 'SHSE.' + code
    else:
        code = 'SZSE.' + code
    return code


def code_to_jue_jin_symbol(code):
    """
    code转换为掘金symbol
    """
    return code_to_shse_szse_symbol(code)


def shse_szse_symbol_to_sh_sz_symbol(symbol):
    if 'SHSE.' in symbol:
        symbol = symbol.replace('SHSE.', "sh")
    elif 'SZSE.' in symbol:
        symbol = symbol.replace('SZSE.', "sz")
    return symbol


def symbol_remove_shse_szse(symbol):
    if 'SHSE.' in symbol:
        symbol = symbol.replace('SHSE.', "")
    elif 'SZSE.' in symbol:
        symbol = symbol.replace('SZSE.', "")
    return symbol


def symbol_remove_SZ_SH(symbol):
    if '.SH' in symbol:
        symbol = symbol.replace('.SH', "")
    elif '.SZ' in symbol:
        symbol = symbol.replace('.SZ', "")
    elif '.SS' in symbol:
        symbol = symbol.replace('.SS', "")
    return symbol


def symbol_remove_SZ_SH_front(symbol):
    if 'SH' in symbol:
        symbol = symbol.replace('SH', "")
    elif 'SZ' in symbol:
        symbol = symbol.replace('SZ', "")
    return symbol


def code_in_symbols(code, symbols):
    """
        是否包含股票代码,code是否包含在symbol中
    """
    symbol_final = None
    for symbol in symbols:
        if operator.contains(symbol, code):
            symbol_final = symbol
    return symbol_final
