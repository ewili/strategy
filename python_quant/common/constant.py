"""
日期格式化为
%Y-%m-%d %H:%M:%S
"""
DATE_FORMAT_PARTTERN_Y_M_D_H_M_S = '%Y-%m-%d %H:%M:%S'

DATE_FORMAT_PARTTERN_Y_M_D = '%Y-%m-%d'

DATE_FORMAT_PARTTERN_YMD = '%Y%m%d'


class BOOL:
    TRUE = '1'
    FALSE = '0'
