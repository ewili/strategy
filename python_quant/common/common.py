import operator

import pandas as pd
import requests
from pandas.io import json


def is_number(s):
    """
    是否为数字
    :param s:
    :return:
    """
    try:
        f = float(s)
        if f != f or f == float('inf') or f == float('-inf'):
            return False
        return True
    except ValueError:
        return False


def xue_qiu_board(url):
    """
    雪球榜单爬虫
    :param url:
    :return:
    """
    result = pd.DataFrame()
    code_list = list()
    # 收集到的常用Header
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36"}
    session = requests.Session()
    session.get('https://xueqiu.com', headers=headers)
    data = session.get(url=url, headers=headers).json()
    data_list = data['data']['list']
    for content in data_list:
        if 'code' in content:
            code_list.append(content['code'])
        elif 'symbol' in content:
            if operator.contains(content['symbol'], 'SH') or operator.contains(content['symbol'], 'SZ'):
                code_ = content['symbol'].replace("SH", "")
                code_ = code_.replace("SZ", "")
                code_list.append(code_)

    result.insert(loc=0, column='代码', value=code_list)
    return result


def ths_billboard(url):
    content = requests.get(url)
    code_list = list()
    if content.status_code != 200:
        return code_list
    data = json.loads(content.text)
    for content in data['data']['stock_list']:
        code_list.append(content['code'])
    return code_list
