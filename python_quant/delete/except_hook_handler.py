# -*- coding: UTF-8 -*-
import sys

from python_quant.common.log import get_logger


## @detail 创建记录异常的信息
class except_hook_handler(object):
    ## @detail 构造函数
    #  @param logFile: log的输入地址
    def __init__(self, logname):
        self.__Logname = logname
        # self.__Logger = self.__BuildLogger()
        # self.__Logger = handle_log(self.__Logname)
        # 重定向异常捕获
        sys.excepthook = self.HandleException

    ## @detail 创建logger类
    def __BuildLogger(self):
        # 日志配置
        # 读取日志配置文件内容
        # 创建一个日志器logger
        return get_logger(self.__Logname)
        # logger = logging.getLogger()
        # logger.setLevel(logging.DEBUG)
        # logger.addHandler(logging.FileHandler(self.__Logname + ".log"))
        # return logger

    ## @detail 捕获及输出异常类
    #  @param excType: 异常类型
    #  @param excValue: 异常对象
    #  @param tb: 异常的trace back
    def HandleException(self, excType, excValue, tb):
        # first logger
        try:
            self.__Logger.error("Uncaught exception：", exc_info=(excType, excValue, tb))
        except:
            pass

        # then call the default handler
        sys.__excepthook__(excType, excValue, tb)

        # err_msg = ('' + currentTime.strftime("%Y-%m-%d %H:%M:%S")).join(traceback.format_exception(excType, excValue, tb))
        # print(err_msg)


if __name__ == '__main__':
    ext = except_hook_handler("hnx")
