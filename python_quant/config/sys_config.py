from urllib.parse import quote_plus


class LoggerConfig:
    logger_name = 'python_api'
    logger_level = 'DEBUG'


class LogMailConfig:
    mailhost = ('smtp.qq.com', 465)
    fromaddr = ('378981649@qq.com',)
    toaddrs = '378981649@qq.com'
    subject = '策略运行出问题了！！！！！'
    credentials = ('378981649@qq.com', 'myzapdzjcsrccbci')


class NotifyMailConfig:
    mailhost = ('smtp.qq.com', 465)
    fromaddr = '378981649@qq.com'
    toaddrs = ['378981649@qq.com']
    subject = '策略运行通知'
    credentials = ('378981649@qq.com', 'myzapdzjcsrccbci')


class MysqlConfig:
    host = "wslhost"
    port = "3306"
    user = "root"
    passwd = quote_plus("Root@ewili8")
    db = "model"


class SQLAlchemyConfig:
    # SQLAlchemy 数据库连接串，格式见下面
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://' + MysqlConfig.user + ':' + MysqlConfig.passwd + '@' + MysqlConfig.host + ':' + MysqlConfig.port + '/' + MysqlConfig.db + '?charset=utf8mb4'
    # 是不是要把所执行的SQL打印出来，一般用于调试
    SQLALCHEMY_ECHO = True
    # 连接池大小
    SQLALCHEMY_POOL_SIZE = 1000
    # 连接池最大的大小
    SQLALCHEMY_POOL_MAX_SIZE = 1000
    # 多久时间主动回收连接
    SQLALCHEMY_POOL_RECYCLE = 3600
