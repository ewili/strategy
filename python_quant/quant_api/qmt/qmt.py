from xtquant import xtdata
from xtquant.xtdata import get_local_data

all_stock_list = xtdata.get_stock_list_in_sector("沪深A股")
for stock in all_stock_list:
    r = xtdata.supply_history_data(stock, 'tick')
    print(r, stock)
    break

data = get_local_data(stock_code=['000606.SZ'], period='tick', count=-1, dividend_type='none',
                      fill_data=True)

print(data)
