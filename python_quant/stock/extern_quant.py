import json

import requests


def dfcf_analysis(code):
    url = 'https://data.eastmoney.com/stockcomment/api/' + code + '.json'
    content = requests.get(url)
    data = json.loads(content.text)
    rise = data['ApiResults']
    return rise
