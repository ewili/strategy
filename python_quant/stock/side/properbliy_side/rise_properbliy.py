import json

import requests


def dfcf_rise_properbliy(code):
    url = 'https://data.eastmoney.com/stockcomment/api/' + code + '.json'
    content = requests.get(url)
    data = json.loads(content.text)
    rise = data['ApiResults']['zhpj']['RiseAndFallPredict'][0]
    return rise


def dfcf_next_five_day_rise_properbliy_more_than_with_out_rose_rate(code, percent):
    rise = dfcf_rise_properbliy(code)
    print(rise['NextFiveDayRoseProbability'])
    if 'NextFiveDayRoseProbability' in rise:
        if float(rise['NextFiveDayRoseProbability']) >= percent and float(rise['NextFiveDayChgAvg']) > 0:
            return True
    return False


def dfcf_next_five_day_rise_properbliy_more_than(code, percent, rose_rate):
    rise = dfcf_rise_properbliy(code)
    if 'NextFiveDayRoseProbability' in rise:
        if float(rise['NextFiveDayRoseProbability']) >= percent and float(rise['NextFiveDayChgAvg']) > rose_rate:
            return True
    return False
