import json

import pandas as pd
import requests


def dfcf_factor_select():
    """
        东方财富条件选股
    """
    url = 'https://data.eastmoney.com/dataapi/xuangu/list?st=CHANGE_RATE&sr=-1&ps=50&p=1&sty=SECUCODE%2CSECURITY_CODE%2CSECURITY_NAME_ABBR%2CNEW_PRICE%2CCHANGE_RATE%2CVOLUME_RATIO%2CHIGH_PRICE%2CLOW_PRICE%2CPRE_CLOSE_PRICE%2CVOLUME%2CDEAL_AMOUNT%2CTURNOVERRATE%2CUPP_DAYS%2CNETINFLOW_3DAYS%2CNOWINTERST_RATIO_3D%2CDDX_3D%2CMARKET%2CBROWSE_RANK&filter=(UPP_DAYS%3E%3D3)(NETINFLOW_3DAYS%3E100000000)(NOWINTERST_RATIO_3D%3E0)(DDX_3D%3E0)(MARKET+in+(%22%E4%B8%8A%E4%BA%A4%E6%89%80%E4%B8%BB%E6%9D%BF%22%2C%22%E6%B7%B1%E4%BA%A4%E6%89%80%E4%B8%BB%E6%9D%BF%22))(BROWSE_RANK%3E0)(BROWSE_RANK%3C%3D1000)&source=SELECT_SECURITIES&client=WEB'
    content_str = requests.get(url)
    content_json = json.loads(content_str.text)
    content_json = content_json['result']['data']
    return pd.json_normalize(content_json)


def dfcf_factor_select_stock():
    dfcf_df = dfcf_factor_select()
    return dfcf_df['SECUCODE'].tolist()


if __name__ == '__main__':
    print(dfcf_factor_select_stock())
