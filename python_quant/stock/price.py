import pandas as pd
import requests

from ..common.date import date_format


def fq_factor(fq_type, code):
    df = pd.DataFrame(columns=['date', 'fq'])
    url = 'https://finance.sina.com.cn/realstock/company/' + code + '/' + fq_type + '.js'
    content = requests.get(url)
    arr = content.text.split('}]')
    # str_content = arr[0].replace("[", "")
    atrt = arr[0].split("data:")
    atrt = atrt[1].replace("{", "")
    atrt = atrt.replace("}", "")
    atrt = atrt.replace("\"", "")
    atrts = atrt.split(',')
    for i in range(len(atrts)):
        content = atrts[i].split(':')
        date = date_format(content[0], "_%Y_%m_%d", "%Y%m%d")
        row = {'date': date, 'fq': float(content[1])}
        df.loc[i] = row
    return df
