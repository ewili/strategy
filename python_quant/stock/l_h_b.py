from datetime import datetime

import akshare as ak
from gm.api import get_previous_trading_date


def lhb_yesterday_clear_buy(context):
    """
    龙虎榜昨日净买入大于0
    :param context:
    :return:
    """
    date3 = context.now.strftime("%Y%m%d")
    pre_date = get_previous_trading_date(exchange='SZSE', date=date3)
    yesterday = datetime.strptime(pre_date, '%Y-%m-%d')
    yesterday = datetime.strftime(yesterday, "%Y%m%d")
    stock_lhb_detail_em_df = ak.stock_lhb_detail_em(start_date=yesterday, end_date=yesterday)
    return stock_lhb_detail_em_df[(stock_lhb_detail_em_df['龙虎榜净买额'] > 0)]
