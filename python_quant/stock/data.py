import json

import requests


def cost_analysis(code, date):
    """
    涨乐通股东成本
    :param code:
    :param date:
    :return:
    """
    url = 'https://m.zhangle.com/push-web/3gGate/gate?path=%2Fapi_reset%2Faverage_cost&stockCode=' + str(
        code) + '&dateTime=' + str(date) + '&_routeCode=6D169C2DF68A80DC82104789FC9AEE27'
    content = requests.get(url)
    data = json.loads(content.text)
    result_data = data['resultData']
    if len(result_data) == 0:
        return {}
    result_data = result_data[0]
    highest_price = result_data['highestPrice']
    lower_to_market_avg_price = result_data['lowerToMarketAvgPrice']
    latest_price = result_data['latestPrice']
    lowest_price = result_data['lowestPrice']
    ten_big_stocker_avg = result_data['tenBigStockerAvg']
    profit_percent = result_data['profitPercent']
    avg_cost_of_carry = result_data['avgCostOfCarry']
    chart_data = result_data['chartData']

    indict = {'highestPrice': highest_price, 'lowerToMarketAvgPrice': lower_to_market_avg_price,
              'latestPrice': latest_price, 'lowestPrice': lowest_price, 'tenBigStockerAvg': ten_big_stocker_avg,
              'profitPercent': profit_percent, 'avgCostOfCarry': avg_cost_of_carry, 'chartData': chart_data}

    return indict


def requests_lb_top20():
    content = requests.get(
        'https://1.push2.eastmoney.com/api/qt/clist/get?pn=1&pz=20&po=1&np=1&ut=bd1d9ddb04089700cf9c27f6f7426281&fltt=2&invt=2&wbp2u=|0|0|0|web&fid=f10&fs=b:BK0655+f:!50&fields=f1,f2,f3,f4,f5,f6,f7,f8,f9,f10,f12,f13,f14,f15,f16,f17,f18,f20,f21,f23,f24,f25,f22,f11,f62,f128,f136,f115,f152,f45&_=1662452865565')
    data = json.loads(content.text)
    data = data['data']
    lb_top20 = list()
    for diff in data['diff']:
        lb_top20.append(diff['f12'])
    return lb_top20


def holder_action_dates():
    url = 'https://datacenter-web.eastmoney.com/api/data/v1/get?callback=jQuery1123026443789952810803_1660622290753&reportName=RPT_FREEHOLDERS_HD_REPORTDATE&columns=END_DATE%2CREPORT_DATE_NAME&sortColumns=END_DATE&sortTypes=-1&source=WEB&client=WEB&pageSize=500&filter=(END_DATE%3E%3D%272015-03-31%27)&_=1660622290754'
    content = requests.get(url)
    arr = content.text.split("(")
    jd = json.loads(arr)
    date_arr = jd['result']['data']['END_DATE']
    return date_arr
